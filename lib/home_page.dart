import 'dart:io';
import 'dart:isolate';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:my_banking_app/constant.dart';
import 'package:my_banking_app/utils/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int progress = 0;
  String savedNumber = SharedPreferencesConstant.accountNumber;
  String path = '/storage/emulated/0/Download/';
  String invoiceNumber = '';
  ReceivePort receivePort = ReceivePort();
  String url =
      'https://www.commercebank.com/-/media/cb/pdf/personal/bank/statement_sample1.pdf';

  @override
  void initState() {
    //initiate callback
    IsolateNameServer.registerPortWithName(receivePort.sendPort, "downloading");
    receivePort.listen((message) {
      setState(() {
        progress = message;
      });
    });
    FlutterDownloader.registerCallback(downloadCallback);
    //check saved numbers
    checkRecentNumber();
    super.initState();
  }
  checkRecentNumber() async {
    String number = await read('accountNumber');
    setState(() {
      savedNumber = number;
    });
  }

  static downloadCallback(id, status, progress) {
    SendPort? sendPort = IsolateNameServer.lookupPortByName('downloading');
    sendPort?.send(progress);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Center(
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Colors.purple.shade800,
                padding: const EdgeInsets.symmetric(horizontal: 50),
              ),
              onPressed: () {
                downloadFile();
              },
              child: const Text(
                "Download",
                style: TextStyle(fontSize: 20),
              ),
            ),
          ),
          File(path + 'sample.pdf').existsSync()
              ? Column(
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    Center(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.purple.shade800,
                          padding: const EdgeInsets.symmetric(horizontal: 50),
                        ),
                        onPressed: () {
                          loadFile();
                        },
                        child: const Text(
                          "Load Invoice Number",
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    )
                  ],
                )
              : Container(),
          const SizedBox(
            height: 50,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Recently Saved  : " + savedNumber),
          ),
        ],
      ),
    );
  }

  downloadFile() async {
    //request permission
    final status = await Permission.storage.request();
    if (status.isGranted) {
      final storage = await getExternalStorageDirectory();
      final id = await FlutterDownloader.enqueue(
          url: url, savedDir: path, fileName: 'sample.pdf');
      File(path + 'sample.pdf').existsSync();
    } else {
      print("permission denied");
    }
  }

  Future<void> saveNumber(String invoiceNumber) async {
    await save("accountNumber", invoiceNumber);
    setState(() {
      savedNumber = invoiceNumber.toString();
    });
  }

  loadFile() async {
    //Load the existing PDF document.
    final PdfDocument document =
        PdfDocument(inputBytes: File(path + 'sample.pdf').readAsBytesSync());

    //Create a new instance of the PdfTextExtractor.
    PdfTextExtractor extractor = PdfTextExtractor(document);

//Extract all the text from a particular page.
    List<TextLine> result = extractor.extractTextLines(startPageIndex: 0);

//Predefined bound.
    Rect textBounds = const Rect.fromLTWH(510, 140, 250, 200);

    for (int i = 0; i < result.length; i++) {
      List<TextWord> wordCollection = result[i].wordCollection;
      for (int j = 0; j < wordCollection.length; j++) {
        if (textBounds.overlaps(wordCollection[j].bounds)) {
          invoiceNumber = wordCollection[j].text;
          break;
        }
      }
      if (invoiceNumber != '') {
        break;
      }
    }

//Display the text.
    _showResult(invoiceNumber);
  }

  void _showResult(String text) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Invoice Number'),
            content: Scrollbar(
              child: SingleChildScrollView(
                child: Text(text),
                physics: const BouncingScrollPhysics(
                    parent: AlwaysScrollableScrollPhysics()),
              ),
            ),
            actions: [
              ElevatedButton(
                  child: const Text('Save'),
                  onPressed: () {
                    saveNumber(invoiceNumber);
                  }),
              ElevatedButton(
                child: const Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}
