import 'package:flutter/material.dart';
import 'package:my_banking_app/constant.dart';
import 'package:my_banking_app/home_page.dart';
import 'package:my_banking_app/utils/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final bool _obscureText = true;
  bool validatePassword = false;
  final _passwordController = TextEditingController();
  final _emailController = TextEditingController();
  bool emailCorrect = true;
  bool passwordCorrect = true;
  String savedEmail = SharedPreferencesConstant.email;
  String savedPassword = SharedPreferencesConstant.password;

  @override
  void initState() {
    saveCredentials();
    super.initState();
  }

  saveCredentials() async {
    await save(SharedPreferencesConstant.email, savedEmail);
    await save(SharedPreferencesConstant.password, savedPassword);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Form(
            key: _formKey,
            child: Container(
              child: Column(
                children: [
                  const SizedBox(
                    height: 100,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          const Text("My Banking App",style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              validator: (value) => value!.isEmpty
                                  ? 'Email is required'
                                  : validateEmail(value),
                              controller: _emailController,
                              keyboardType: TextInputType.emailAddress,
                              decoration: const InputDecoration(
                                hintText: "Email",
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextFormField(
                              controller: _passwordController,
                              obscureText: _obscureText,
                              decoration: const InputDecoration(
                                hintText: "Password",
                              ),
                            ),
                          ),
                          ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Colors.purple.shade800,
                              padding: EdgeInsets.symmetric(horizontal: 50),
                            ),
                            onPressed: () {
                              _validateInputs();
                            },
                            child: const Text(
                              "Login",
                              style: TextStyle(fontSize: 20),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  void _validateInputs() {
    if (_formKey.currentState!.validate()) {
      userLogin();
    } else {}
  }

  String? validateEmail(String value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = RegExp(pattern.toString());
    if (!regex.hasMatch(value) || value == null) {
      return 'invalid email format';
    } else {
      return null;
    }
  }

  userLogin() {
    read(SharedPreferencesConstant.email).then((email) {
      read(SharedPreferencesConstant.password).then((password) {
        if (email == _emailController.text &&
            password == _passwordController.text) {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const HomePage()));
        }
      });
    });
  }
}
