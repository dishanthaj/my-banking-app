import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

save(String key, value) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString(key, json.encode(value));
}
//int intValue= await prefs.getInt('intValue') ?? 0;
Future read(String key) async {
  final prefs = await SharedPreferences.getInstance();
  return json.decode(prefs.getString(key) ?? '');
}